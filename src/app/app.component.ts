// import { Component } from '@angular/core';
//
// @Component({
//   selector: 'app-root',
//   templateUrl: './app.component.html',
//   styleUrl: './app.component.scss'
// })
// export class AppComponent {
//   title = 'angular_routing';
//
//
//   isDarkTheme = false;
//
//   toggleTheme() {
//     this.isDarkTheme = !this.isDarkTheme;
//   }
// }

import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  // styleUrls: ['./app.component.css'] // Uncomment if you have styles defined in this file
})
export class AppComponent {
  // title = 'customer-app';
}
