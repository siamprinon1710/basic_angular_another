// import {Component, OnInit} from '@angular/core';
// import {FormArray, FormBuilder, FormControl, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";
// import { Router } from "@angular/router";
// import {NgForOf, NgIf} from "@angular/common"; // Corrected import
//
// @Component({
//   selector: 'app-loan-types',
//   templateUrl: './loan-types.component.html',
//   standalone: true,
//   imports: [
//     ReactiveFormsModule,
//     NgIf,
//     NgForOf
//   ],
//   styleUrls: ['./loan-types.component.scss'] // Corrected from 'styleUrl' to 'styleUrls'
// })
// export class LoanTypesComponent implements OnInit {
//   addLoanTypesForm: FormGroup = new FormGroup({});
//   private data: any;
//
//   constructor(private fb: FormBuilder, private router: Router) {
//   } // Ensure Router is used appropriately
//
//   ngOnInit(): void {
//
//     // use case #1 -> defining a simple FormArray
//
//     let users = new FormArray([
//       new FormControl('Siam'),
//       new FormControl('Startup')
//     ]);
//
//   console.log(users);
//   console.log(users.value);
//
//
//     this.addLoanTypesForm = this.fb.group({
//       'loanName': new FormControl('', [
//         Validators.required,
//         Validators.minLength(10),
//         Validators.maxLength(20)
//       ]),
//       'loanType': new FormControl(''),
//       'loanDescription': new FormControl('', Validators.compose([
//         Validators.required,
//         Validators.minLength(10),
//         Validators.maxLength(20)
//       ])),
//
//       users: new FormArray([
//         new FormControl('Siam'),
//         new FormControl('Startup')
//       ])
//
//     });
//
//     this.addLoanTypesForm.statusChanges.subscribe(data=>{
//       console.log("Form Status", data);
//       console.log(data);
//     })
//
//
//     // this.addLoanTypesForm.get('loanName')!.statusChanges.subscribe(data => {
//     //   console.log(data);
//     // })
//
//
//   }
//
//   addLoanType() {
//     console.log(this.addLoanTypesForm.valid);
//     this.addLoanTypesForm.valueChanges.subscribe(data => {
//       this.data = data; // Assign the form value to this.data
//       console.log(this.data);
//     });
//   }
//
//   resetForm(): void {
//     this.addLoanTypesForm.reset();
//   }
//
//   trackLoanName(): void {
//     this.addLoanTypesForm.valueChanges.subscribe(data => {
//
//       console.log(this.addLoanTypesForm.value);
//       // console.log(this.data);
//     })
//   }
// }


import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";

@Component({
  selector: 'app-loan-types',
  templateUrl: './loan-types.component.html',
  standalone: true,
  imports: [
    ReactiveFormsModule
  ],
  styleUrls: ['./loan-types.component.scss']
})
export class LoanTypesComponent implements OnInit {
  addLoanTypesForm: FormGroup;
  private data: any;

  constructor(private fb: FormBuilder) {
    this.addLoanTypesForm = this.fb.group({
      loanName: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(20)]],
      loanType: [''],
      loanDescription: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(20)]],
      users: this.fb.array([])
    });
  }

  ngOnInit(): void {
    this.addLoanTypesForm.statusChanges.subscribe(data => {
      console.log("Form Status", data);
    });

    this.addLoanTypesForm.valueChanges.subscribe(data => {
      this.data = data;
      console.log(this.data);
    });
  }

  get usersControls() {
    return (this.addLoanTypesForm.get('users') as FormArray).controls;
  }

  addUser() {
    const users = this.addLoanTypesForm.get('users') as FormArray;
    users.push(this.createFormGroupForUser());
  }

  createFormGroupForUser(): FormGroup {
    return this.fb.group({
      name: ['', Validators.required],
      age: [''],
      dept: ['']
    });
  }

  removeUser(index: number) {
    const users = this.addLoanTypesForm.get('users') as FormArray;
    users.removeAt(index);
  }

  addLoanType() {
    console.log(this.addLoanTypesForm.valid);
    this.data = this.addLoanTypesForm.value;
    console.log(this.data);
  }

  resetForm(): void {
    this.addLoanTypesForm.reset();
  }

  trackLoanName(): void {
    console.log(this.addLoanTypesForm.value);
  }
}




