import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})


export class authGuard implements CanActivate {

  userToken = false;
  // a =0;
  // b = 0;
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    // this.a = 100;
    // this.b = 20;

      this.userToken = true;

    if (this.userToken) {
      return true;
    }

    else {
      return false;
    }

  }
}
