import { TestBed } from '@angular/core/testing';
import { CanActivateFn } from '@angular/router';

import { preferencesCheckGuard } from './preferences-check.guard';

describe('preferencesCheckGuard', () => {
  const executeGuard: CanActivateFn = (...guardParameters) => 
      TestBed.runInInjectionContext(() => preferencesCheckGuard(...guardParameters));

  beforeEach(() => {
    TestBed.configureTestingModule({});
  });

  it('should be created', () => {
    expect(executeGuard).toBeTruthy();
  });
});
