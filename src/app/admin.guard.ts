// import { Injectable } from '@angular/core';
// import { CanActivateFn } from '@angular/router';
// import { Observable } from 'rxjs';
//
// @Injectable({
//   providedIn: 'root'
// })
//
//
// export const adminGuard: CanActivateFn {
//   canActivate()
//   {
//     const isAdmin = false;
//     if (isAdmin) {
//       return true;
//     }
//     else {
//       return false;
//     }
//   }
// };

import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean | Observable<boolean> | Promise<boolean> {
    const isAdmin = true; // Replace this with your actual logic
    if (isAdmin) {
      return true;
    } else {
      return false;
    }
  }
}

