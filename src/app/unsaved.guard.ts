// import {CanActivateFn} from '@angular/router';
// import {SearchComponent} from "./search/search.component";
// import {Injectable} from "@angular/core";
//
// @Injectable({
//   providedIn: 'root'
// })
//
// export const unsavedGuard: CanActivateFn<SearchComponent>
// canDeactivate(component
// :
// SearchComponent
// )
// {
//   if (component.isDirty) {
//     return window.confirm("YOU HAVE SOME UNSAVED CHANGES.SURE YOU WANT TO LEAVE?");
//   }
//   return true;
// }
//
// }
//

import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { SearchComponent } from './search/search.component';

@Injectable({
  providedIn: 'root'
})
export class UnsavedGuard implements CanDeactivate<SearchComponent> {
  canDeactivate(component: SearchComponent): boolean {
    console.log(component);
    console.log(component.isDirty);
    if (component.isDirty) {
       window.alert("YOU HAVE SOME UNSAVED CHANGES. SURE YOU WANT TO LEAVE?");
    }
    return true;
  }
}


