import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrl: './clients.component.scss'
})
export class ClientsComponent implements OnInit {

  clientList =[
    { clientId: 1, firstName: 'Mota', lastName: 'King' },
    { clientId: 2, firstName: 'Boby', lastName: 'Ding' },
    { clientId: 3, firstName: 'Forid', lastName: 'Ring' },
    { clientId: 4, firstName: 'Bilash', lastName: 'Ting' },
    { clientId: 5, firstName: 'Kalu', lastName: 'Fing' },



  ];

  constructor(){}
  ngOnInit() {
  }

}
