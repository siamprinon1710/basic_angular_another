// import {Component, OnInit} from '@angular/core';
//
// @Component({
//   selector: 'app-add-customer',
//   templateUrl: './add-customer.component.html',
//   styleUrl: './add-customer.component.scss'
// })
// export class AddCustomerComponent implements OnInit{
//
//   firstname: string = '';
//   constructor() {}
//
//   ngOnInit() {
//   }
// }

import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-add-customer',
  templateUrl: './add-customer.component.html',
  styleUrls: ['./add-customer.component.scss']
})
export class AddCustomerComponent implements OnInit {

  firstname: string = '';
  terms: boolean = false;
  customerType: string = '';

  constructor() {}

  ngOnInit() {
    }

    loadValues(formValue: NgForm){

      let userDetails = {
        firstname: 'Don',
        terms: false,
        customerType: '1',
        description: ''

    }

    formValue.setValue(userDetails);

  }

  addCustomer(form: NgForm) {
    console.log(form.value);
  }

  resetForm(formValue: NgForm): void {
    formValue.reset();
  }

}


