import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrl: './about.component.scss'
})
export class AboutComponent implements OnInit {

    constructor(private ActivatedRoute: ActivatedRoute) { }
  ngOnInit() {
      console.log(this.ActivatedRoute.snapshot.data);
  }
}
