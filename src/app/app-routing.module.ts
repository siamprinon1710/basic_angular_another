// import {NgModule} from '@angular/core';
// import {RouterModule, Routes} from '@angular/router';
// import {LoansComponent} from "./loans/loans.component";
// import {AppComponent} from "./app.component";
// import {AboutComponent} from "./about/about.component";
// import {AddComponent} from "./add/add.component";
// import {LoanTypesComponent} from "./loan-types/loan-types.component";
// import {ProductComponent} from "./product/product.component";
// import {ClientsComponent} from "./clients/clients.component";
// import {SearchComponent} from "./search/search.component";
// import {PageNotFoundComponent} from "./page-not-found/page-not-found.component";
// import {authGuard} from "./auth.guard";
// import {AdminGuard} from "./admin.guard";
// import {AdminComponent} from "./admin/admin.component";
// import {AdminManageComponent} from "./admin-manage/admin-manage.component";
// import {AdminEditComponent} from "./admin-edit/admin-edit.component";
// import {AdminDeleteComponent} from "./admin-delete/admin-delete.component";
//
// function SuperAdminGuard() {
//
// }
//
// function AdminAccessGuard() {
//
// }
//
// function PreferencesCheckGuard() {
//
// }
//
// function UnsavedGuard() {
//
// }
//
// function AccountInfoGuard() {
//
// }
//
// const routes: Routes = [
//   // { path: '', component: AppComponent, outlet: 'home' },
//   // { path: 'add-new-loan', redirectTo: 'about'},
//   //
//   // { path: 'loans', component: LoansComponent },
//   // { path: 'about', component: AboutComponent },
//   // { path: 'add', component: AddComponent, outlet: 'addLoan' },
//   // { path: 'loan-types', component: LoanTypesComponent, children: [{
//   //     path: 'loans', component: LoansComponent
//   //
//   // }
//   //   ]
//   // }
//
//   {path: 'product/:id', component: ProductComponent},
//   {path: 'product/:productId/photos/:photoId', component: ProductComponent},
//
//   {
//     path: 'clients',
//     component: ClientsComponent,
//     canActivate: [AdminGuard, authGuard],
//   },
//
//   {
//     path: '',
//     redirectTo: 'about',
//     pathMatch: "full"
//   },
//
//   {
//     path: 'about',
//     component: AboutComponent,
//     resolve: {
//       data: AccountInfoGuard
//     }
//   },
//
//   {
//     path: 'admin',
//     canActivate: [SuperAdminGuard],
//
//     children: [
//       {
//         path: '',
//         component: AdminComponent,
//
//       },
//
//       {
//         path: '',
//         canActivateChild: [AdminAccessGuard],
//         children: [
//           {path: 'manage', component: AdminManageComponent},
//           {path: 'edit', component: AdminEditComponent},
//           {path: 'delete', component: AdminDeleteComponent},
//         ]
//       }
//
//
//     ]
//   },
//
//   {
//     path: 'search',
//     component: SearchComponent,
//     canDeactivate: [UnsavedGuard]
//   },
//   {path: 'payments', loadChildren: () => import('./payments/payments.module').then(m => m.PaymentsModule)},
//   {path: 'payments', loadChildren: () => import('./customer/customer.module').then(m => m.CustomerModule)},
//   { path: 'preferences',
//     canMatch: [PreferencesCheckGuard],
//     loadChildren: () => import('./preferences/preferences.module').then(m => m.PreferencesModule) },
//
//   {
//     path: '**',
//     component: PageNotFoundComponent
//   },
//
// ];
//
// @NgModule({
//   imports: [RouterModule.forRoot(routes)],
//   exports: [RouterModule]
// })
// export class AppRoutingModule {
// }

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoanTypesComponent} from "./loan-types/loan-types.component";

const routes: Routes = [
  { path: 'customer', loadChildren: () => import('./customer/customer.module').then(m => m.CustomerModule) },
  {path: 'loan-types', component: LoanTypesComponent},
  { path: '', redirectTo: 'customer', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
