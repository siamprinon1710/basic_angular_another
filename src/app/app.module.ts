// import { NgModule } from '@angular/core';
// import { BrowserModule, provideClientHydration } from '@angular/platform-browser';
//
// import { AppRoutingModule } from './app-routing.module';
// import { AppComponent } from './app.component';
// import { LoansComponent } from './loans/loans.component';
// import { AboutComponent } from './about/about.component';
// import { AddComponent } from './add/add.component';
// import { LoanTypesComponent } from './loan-types/loan-types.component';
//
// import {HashLocationStrategy, LocationStrategy, PathLocationStrategy} from "@angular/common";
//
// import { ProductComponent } from './product/product.component';
// import { ClientsComponent } from './clients/clients.component';
// import { SearchComponent } from './search/search.component';
// import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
// import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';
// import {MatCard, MatCardContent} from "@angular/material/card";
// import {MatSlideToggle} from "@angular/material/slide-toggle";
// import {MatToolbar} from "@angular/material/toolbar";
// import {MatIcon} from "@angular/material/icon";
// import {MatToolbarModule} from '@angular/material/toolbar';
// import {MatCardModule} from '@angular/material/card';
// import {MatSlideToggleModule} from '@angular/material/slide-toggle';
// import {MatIconModule} from '@angular/material/icon';
//
//
//
//
// @NgModule({
//   declarations: [
//     AppComponent,
//     LoansComponent,
//     AboutComponent,
//     AddComponent,
//     LoanTypesComponent,
//     ProductComponent,
//     ClientsComponent,
//     SearchComponent,
//     PageNotFoundComponent
//   ],
//   imports: [
//     BrowserModule,
//     AppRoutingModule,
//     MatCard,
//     MatCardContent,
//     MatSlideToggle,
//     MatToolbar,
//     MatIcon,
//     MatToolbarModule,
//     MatCardModule,
//     MatSlideToggleModule,
//     MatIconModule
//   ],
//   providers: [
//     provideClientHydration(),
//     {
//       provide: LocationStrategy, useClass: PathLocationStrategy
//     },
//     provideAnimationsAsync()
//   ],
//   bootstrap: [AppComponent]
// })
// export class AppModule { }

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoanTypesComponent } from './loan-types/loan-types.component'; // Import the standalone component

@NgModule({
  declarations: [
    AppComponent
    // LoanTypesComponent is not declared here because it's standalone
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    ReactiveFormsModule
    // LoanTypesComponent // Import standalone component directly
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }




